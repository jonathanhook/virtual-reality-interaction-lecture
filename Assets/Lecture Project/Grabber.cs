﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class Grabber : MonoBehaviour
{
    public float pinchThreshold = 20.0f;

    private bool grabbed = false;
    private IHandModel hand = null;
    private GameObject target = null;

    void Awake()
    {
        hand = GetComponentInParent<IHandModel>();
    }
	
	void Update ()
    {
        if(hand != null)
        {
            bool isPinching = hand.GetLeapHand().PinchDistance < pinchThreshold;

            if (!grabbed && isPinching && target != null)
            {
                target.transform.parent = transform;
            }
            else if(grabbed && !isPinching)
            {
                target.transform.parent = null;
            }
            
        }   
    }

    void OnCollisionEnter(Collision c)
    {
        Debug.Log("In");
        target = c.collider.gameObject;
    }

    void OnCollisionExit(Collision c)
    {
        Debug.Log("Out");
        target = null;
    }
}
