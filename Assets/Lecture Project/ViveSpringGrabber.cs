﻿using UnityEngine;
using System.Collections;

public class ViveSpringGrabber : MonoBehaviour
{
    private SteamVR_TrackedObject controller;
    private GameObject target = null;

    private SpringJoint joint;

    void Start ()
    {
        controller = GetComponent<SteamVR_TrackedObject>();
        joint = GetComponent<SpringJoint>();
    }
	
	void Update ()
    {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);

        if (joint.connectedBody == null && target != null && device.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
        {
            joint.connectedBody = target.gameObject.GetComponent<Rigidbody>();
        }
        else if (joint.connectedBody != null && device.GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
        {
            joint.connectedBody = null;
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (target == null)
        {
            target = c.gameObject;
        }
    }

    void OnTriggerExit(Collider c)
    {
        if (c.gameObject == target)
        {
            target = null;
        }
    }
}
