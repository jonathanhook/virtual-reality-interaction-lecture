﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class ViveGrabber : MonoBehaviour
{
    private SteamVR_TrackedObject controller;
    private GameObject grabbed = null;
    private GameObject target = null;
    private Rigidbody rb;

    void Start ()
    {
        controller = GetComponent<SteamVR_TrackedObject>();
        rb = GetComponent<Rigidbody>();
    }
	
	void Update ()
    {
        SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.index);

        if (grabbed == null && target != null && device.GetPressDown(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
        {
            Debug.Log(target);

            grabbed = target;
            grabbed.GetComponent<Rigidbody>().isKinematic = true;
            grabbed.transform.parent = transform;
        }
        else if(grabbed != null && device.GetPressUp(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
        {
            grabbed.transform.parent = null;

            Rigidbody grb = grabbed.GetComponent<Rigidbody>();
            grb.isKinematic = false;
            grb.velocity = device.velocity;
            grb.angularVelocity = device.angularVelocity;

            grabbed = null;
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if(target == null)
        {
            target = c.gameObject;
        }
    }

    void OnTriggerExit(Collider c)
    {
        if(c.gameObject == target)
        {
            target = null;
        }
    }
}
