﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

public class LeapGrabber : MonoBehaviour
{
    public float pinchThreshold = 20.0f;

    private IHandModel controller;
    private GameObject grabbed = null;
    private GameObject target = null;
    private Rigidbody rb;

    void Start ()
    {
        controller = GetComponentInParent<IHandModel>();
        rb = GetComponent<Rigidbody>();
    }
	
	void Update ()
    {
        bool isPinching = controller.GetLeapHand().PinchDistance < pinchThreshold;

        if (grabbed == null && target != null && isPinching)
        {
            grabbed = target;
            grabbed.GetComponent<Rigidbody>().isKinematic = true;
            grabbed.transform.parent = transform;
        }
        else if(grabbed != null && !isPinching)
        {
            grabbed.transform.parent = null;
            
            Rigidbody grb = grabbed.GetComponent<Rigidbody>();
            grb.isKinematic = false;
            //grb.velocity = device.velocity;
            //grb.angularVelocity = device.angularVelocity;
            
            grabbed = null;
        }
    }

    void OnTriggerEnter(Collider c)
    {
        Debug.Log("In");

        if (target == null)
        {
            target = c.gameObject;
        }
    }

    void OnTriggerExit(Collider c)
    {
        Debug.Log("Out");

        if (c.gameObject == target)
        {
            target = null;
        }
    }
}
